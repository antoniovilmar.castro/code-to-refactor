package com.example.demo.infrastructure.repository;

import com.example.demo.domain.Conta;
import org.springframework.stereotype.Repository;

@Repository
public class ContaRepository {

  private ContaSpringData data;

  public ContaRepository(ContaSpringData data) {
    this.data = data;
  }

  public Conta obter(long numeroConta) {
    return this.data.getById(numeroConta);
  }

  public Conta salvar(Conta conta) {
    return this.data.save(conta);
  }
}
