package com.example.demo.infrastructure.repository;

import com.example.demo.domain.Conta;
import org.springframework.data.jpa.repository.JpaRepository;

interface ContaSpringData extends JpaRepository<Conta, Long> {

}