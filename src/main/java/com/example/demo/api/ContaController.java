package com.example.demo.api;

import com.example.demo.application.ContaCorrentePoupancaServiceImpl;
import com.example.demo.domain.TipoConta;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ContaController {
    private ContaCorrentePoupancaServiceImpl contaCorrentePoupancaServiceImpl;

    public ContaController(ContaCorrentePoupancaServiceImpl contaCorrentePoupancaServiceImpl) {
        this.contaCorrentePoupancaServiceImpl = contaCorrentePoupancaServiceImpl;
    }

    @PostMapping(value = "/v1/conta-corrente")
    public ResponseEntity abrirContaCorrente(@RequestBody DadosContaDto dadosContaDto) {
        long numeroConta = this.contaCorrentePoupancaServiceImpl.abrirConta(dadosContaDto, TipoConta.CORRENTE);
        return ResponseEntity.created(null).body(numeroConta);

    }

    @PostMapping(value = "/v1/conta-investimento")
    public ResponseEntity abrirContaInvestimento(@RequestBody DadosContaDto dadosContaDto) {
        long numeroConta = this.contaCorrentePoupancaServiceImpl.abrirConta(dadosContaDto, TipoConta.INVESTIMENTO);
        return ResponseEntity.created(null).body(numeroConta);
    }

    @PostMapping(value = "/v1/conta-poupanca")
    public ResponseEntity abrirContaPoupanca(@RequestBody DadosContaDto dadosContaDto) {
        long numeroConta = this.contaCorrentePoupancaServiceImpl.abrirConta(dadosContaDto, TipoConta.POUPANCA);
        return ResponseEntity.created(null).body(numeroConta);

    }

    @PostMapping(value = "/v1/conta/{numeroConta}/movimentacao")
    public ResponseEntity movimentar(@PathVariable long numeroConta, @RequestBody MovimentacaoContaDto movimentacaoContaDto) {
        this.contaCorrentePoupancaServiceImpl.movimentar(numeroConta, movimentacaoContaDto);
        return ResponseEntity.created(null).build();
    }

    @GetMapping(value = "/v1/conta-extrato/{numeroConta}")
    public ResponseEntity gerarExtrato(@PathVariable long numeroConta) {
        final List<String> extrato = this.contaCorrentePoupancaServiceImpl.gerarExtrato(numeroConta);
        return ResponseEntity.ok().body(extrato);

    }
}
