package com.example.demo.domain;

public class ContaInvestimento extends Conta {

  public ContaInvestimento(ValidacaoAberturaConta validacaoAberturaConta, String cpfTitular,
      long numeroConta) {
    super(validacaoAberturaConta, cpfTitular, TipoConta.INVESTIMENTO, numeroConta, 1L);
  }

  @Override
  public void incluirDependente(String cpf) {
    throw new DomainBusinessException("Conta Investimento não pode ter dependente");
  }
}
