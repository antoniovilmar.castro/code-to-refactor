package com.example.demo.domain;

import com.example.demo.infrastructure.repository.RestricaoRepository;
import org.springframework.stereotype.Service;

@Service
public class ValidacaoAberturaContaInvestimentoImpl implements ValidacaoAberturaConta {

  private RestricaoRepository restricaoRepository;

  public ValidacaoAberturaContaInvestimentoImpl(RestricaoRepository restricaoRepository) {
    this.restricaoRepository = restricaoRepository;
  }

  @Override
  public void validar(String cpf) {
  }
}
