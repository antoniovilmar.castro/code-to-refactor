package com.example.demo.application;

import com.example.demo.api.DadosContaDto;
import com.example.demo.api.MovimentacaoContaDto;
import com.example.demo.domain.Conta;
import com.example.demo.domain.ContaInvestimento;
import com.example.demo.domain.Movimentacao;
import com.example.demo.domain.Restricao;
import com.example.demo.domain.TipoConta;
import com.example.demo.domain.TipoMovimentacao;
import com.example.demo.domain.TipoRestricao;
import com.example.demo.domain.ValidacaoAberturaContaInvestimentoImpl;
import com.example.demo.infrastructure.MovimentacaoRepository;
import com.example.demo.infrastructure.external.GeradorNumeroContaInvestimento;
import com.example.demo.infrastructure.repository.ContaRepository;
import com.example.demo.infrastructure.repository.RestricaoRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class ContaInvestimentoServiceImpl implements ContaService {

  private ContaRepository contaRepository;
  private GeradorNumeroContaInvestimento geradorNumeroContaInvestimento;
  private MovimentacaoRepository movimentacaoRepository;
  private ValidacaoAberturaContaInvestimentoImpl validacaoAberturaContaInvestimento;
  private RestricaoRepository restricaoRepository;

  public ContaInvestimentoServiceImpl(ContaRepository contaRepository,
      GeradorNumeroContaInvestimento geradorNumeroContaInvestimento,
      MovimentacaoRepository movimentacaoRepository,
      ValidacaoAberturaContaInvestimentoImpl validacaoAberturaContaInvestimento,
      RestricaoRepository restricaoRepository) {
    this.contaRepository = contaRepository;
    this.geradorNumeroContaInvestimento = geradorNumeroContaInvestimento;
    this.movimentacaoRepository = movimentacaoRepository;
    this.validacaoAberturaContaInvestimento = validacaoAberturaContaInvestimento;
    this.restricaoRepository = restricaoRepository;
  }

  @Override
  public long abrirConta(final DadosContaDto dadosContaDto,
      final TipoConta tipoConta) {
    long numeroContaInvestimento = geradorNumeroContaInvestimento.gerar(
        dadosContaDto.getCpfTitular());
    ContaInvestimento contaInvestimento = new ContaInvestimento(validacaoAberturaContaInvestimento,
        dadosContaDto.getCpfTitular(),
        numeroContaInvestimento);
    Conta conta = contaRepository.salvar(
        contaInvestimento);

    return conta.getNumero();
  }

  @Override
  public void movimentar(final long numeroConta,
      final MovimentacaoContaDto movimentacaoContaDto) {
    Conta conta = contaRepository.obter(numeroConta);

    if (TipoMovimentacao.SAQUE.equals(movimentacaoContaDto.getMovimento())) {
      conta.sacar(movimentacaoContaDto.getValor());
    } else {
      conta.depositar(movimentacaoContaDto.getValor());
    }

    contaRepository.salvar(conta);
    Movimentacao movimentacao = new Movimentacao(conta.getNumero(),
        conta.getAgencia(),
        conta.getSaldo(), movimentacaoContaDto.getValor(),
        conta.getTipoConta(),
        movimentacaoContaDto.getMovimento());
    movimentacaoRepository.save(movimentacao);

  }

  @Override
  public void incluirDependente(long numeroConta, DadosContaDto dadosContaDto) {
    throw new OperacaoInvalidaException(
        "Não é possível incluir dependente para conta de investimento");
  }

  @Override
  public List<String> gerarExtrato(long numeroConta) {
    return this.movimentacaoRepository.findByNumeroConta(numeroConta).stream()
        .map(Movimentacao::toString).collect(Collectors.toUnmodifiableList());
  }

  @Override
  public List<TipoRestricao> listarRestricoes(String cpf) {
    return this.restricaoRepository.listar(cpf).stream().map(Restricao::getTipoRestricao).collect(
        Collectors.toList());
  }
}
