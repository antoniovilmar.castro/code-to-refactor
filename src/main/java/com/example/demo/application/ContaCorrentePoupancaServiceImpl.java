package com.example.demo.application;

import static java.util.Objects.nonNull;

import com.example.demo.api.DadosContaDto;
import com.example.demo.api.MovimentacaoContaDto;
import com.example.demo.domain.Conta;
import com.example.demo.domain.ContaInvestimento;
import com.example.demo.domain.Movimentacao;
import com.example.demo.domain.Restricao;
import com.example.demo.domain.TipoConta;
import com.example.demo.domain.TipoMovimentacao;
import com.example.demo.domain.TipoRestricao;
import com.example.demo.domain.ValidacaoAberturaContaCorrenteImpl;
import com.example.demo.domain.ValidacaoAberturaContaInvestimentoImpl;
import com.example.demo.infrastructure.ContaInvestimentoRepository;
import com.example.demo.infrastructure.MovimentacaoRepository;
import com.example.demo.infrastructure.external.ConsultaPerfilRisco;
import com.example.demo.infrastructure.external.GeradorNumeroConta;
import com.example.demo.infrastructure.external.GeradorNumeroContaInvestimento;
import com.example.demo.infrastructure.repository.ContaRepository;
import com.example.demo.infrastructure.repository.LimiteRepository;
import com.example.demo.infrastructure.repository.RestricaoRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class ContaCorrentePoupancaServiceImpl implements ContaService {

  private ContaRepository contaRepository;
  private GeradorNumeroConta geradorNumeroConta;
  private GeradorNumeroContaInvestimento geradorNumeroContaInvestimento;
  private RestricaoRepository restricaoRepository;
  private MovimentacaoRepository movimentacaoRepository;
  private ConsultaPerfilRisco consultaPerfilRisco;
  private LimiteRepository limiteRepository;
  private ContaInvestimentoRepository contaInvestimentoRepository;
  private ValidacaoAberturaContaCorrenteImpl validacaoAberturaContaCorrente;
  private ValidacaoAberturaContaInvestimentoImpl validacaoAberturaContaInvestimento;

  public ContaCorrentePoupancaServiceImpl(ContaRepository contaRepository,
      GeradorNumeroConta geradorNumeroConta,
      GeradorNumeroContaInvestimento geradorNumeroContaInvestimento,
      RestricaoRepository restricaoRepository, MovimentacaoRepository movimentacaoRepository,
      ConsultaPerfilRisco consultaPerfilRisco, LimiteRepository limiteRepository,
      ContaInvestimentoRepository contaInvestimentoRepository,
      ValidacaoAberturaContaCorrenteImpl validacaoAberturaContaCorrente,
      ValidacaoAberturaContaInvestimentoImpl validacaoAberturaContaInvestimento) {
    this.contaRepository = contaRepository;
    this.geradorNumeroConta = geradorNumeroConta;
    this.geradorNumeroContaInvestimento = geradorNumeroContaInvestimento;
    this.restricaoRepository = restricaoRepository;
    this.movimentacaoRepository = movimentacaoRepository;
    this.consultaPerfilRisco = consultaPerfilRisco;
    this.limiteRepository = limiteRepository;
    this.contaInvestimentoRepository = contaInvestimentoRepository;
    this.validacaoAberturaContaCorrente = validacaoAberturaContaCorrente;
    this.validacaoAberturaContaInvestimento = validacaoAberturaContaInvestimento;
  }

  @Override
  public long abrirConta(final DadosContaDto dadosContaDto,
      final TipoConta tipoConta) {
    if (TipoConta.INVESTIMENTO.equals(tipoConta)) {
      long numeroContaInvestimento = geradorNumeroContaInvestimento.gerar(
          dadosContaDto.getCpfTitular());
      ContaInvestimento contaInvestimento = new ContaInvestimento(
          validacaoAberturaContaInvestimento,
          dadosContaDto.getCpfTitular(),
          numeroContaInvestimento);
      Conta conta = contaRepository.salvar(
          contaInvestimento);

      return conta.getNumero();
    } else {
      long numeroConta = geradorNumeroConta.gerar(tipoConta);
      var conta = new Conta(validacaoAberturaContaCorrente,
          dadosContaDto.getCpfTitular(), tipoConta,
          numeroConta,
          dadosContaDto.getAgencia());
      if (TipoConta.CORRENTE.equals(tipoConta)) {
        if (nonNull(dadosContaDto.getDependente())) {
          conta.incluirDependente(dadosContaDto.getDependente());
        }
      }
      Conta contaCriada = contaRepository.salvar(conta);

      return contaCriada.getNumero();

    }
  }

  @Override
  public void movimentar(final long numeroConta,
      final MovimentacaoContaDto movimentacaoContaDto) {
    Conta conta = contaRepository.obter(numeroConta);

    if (TipoMovimentacao.SAQUE.equals(movimentacaoContaDto.getMovimento())) {
      conta.sacar(movimentacaoContaDto.getValor());
    } else {
      conta.depositar(movimentacaoContaDto.getValor());
    }

    contaRepository.salvar(conta);
    Movimentacao movimentacao = new Movimentacao(conta.getNumero(),
        conta.getAgencia(),
        conta.getSaldo(), movimentacaoContaDto.getValor(),
        conta.getTipoConta(),
        movimentacaoContaDto.getMovimento());
    movimentacaoRepository.save(movimentacao);


  }

  @Override
  public void incluirDependente(long numeroConta, DadosContaDto dadosContaDto) {
    Conta conta = contaRepository.obter(numeroConta);
    if (TipoConta.CORRENTE.equals(conta.getTipoConta())) {
      if (nonNull(dadosContaDto.getDependente())) {
        conta.incluirDependente(dadosContaDto.getDependente());
      }
    }
  }

  @Override
  public List<String> gerarExtrato(long numeroConta) {
    return this.movimentacaoRepository.findByNumeroConta(numeroConta).stream()
        .map(Movimentacao::toString).collect(Collectors.toUnmodifiableList());
  }


  @Override
  public List<TipoRestricao> listarRestricoes(String cpf) {
    return this.restricaoRepository.listar(cpf).stream().map(Restricao::getTipoRestricao).collect(
        Collectors.toList());
  }

}
